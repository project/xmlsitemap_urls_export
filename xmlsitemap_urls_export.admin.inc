<?php

function xmlsitemap_urls_export_form() {
  return array(
    '#prefix' => '<div id="raport-css">',
    '#suffix' => '</div>',
    'export' =>  array(
      '#type' => 'submit',
      '#value' => t('Export'),
    ),
    'table' => xmlsitemap_urls_export_overview(),
  );
}

function xmlsitemap_urls_export_form_submit() {
  _xmlsitemap_urls_export_download_csv();
}

function xmlsitemp_urls_get_path_alias($location, $langcode) {
  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.generate');
  return xmlsitemap_get_path_alias($location, $langcode);
}

function _xmlsitemap_url_export_get_items($absolute = TRUE) {
  return array_merge(xmlsitemap_urls_prepare_url_files($absolute), xmlsitemap_urls_prepare_urls($absolute));
}

function xmlsitemap_urls_prepare_urls($absolute = TRUE) {
  $header = array(
    array('data' => t('Location'), 'field' => 'x.loc'),
    array('data' => t('Id'), 'field' => 'x.id'),
    array('data' => t('Type'), 'field' => 'x.type'),
    array('data' => t('Language')),
  );

  $query = db_select('xmlsitemap', 'x');
  $query->fields('x', [
    'loc',
    'id',
    'type',
    'language',
  ]);

  $query->condition('status', 1);
  $query->extend('TableSort')->orderByHeader($header);
  $links = $query->execute();
  while ($link = $links->fetchAssoc()) {
    if (in_array($link['loc'], ['<front>', ''])) {
      continue;
    }

    $parsed_url = drupal_parse_url($link['loc']);
    $link['loc'] = $parsed_url['path'];
    $link['loc'] = xmlsitemp_urls_get_path_alias($link['loc'], $link['language']->language);
    $item = array(
      'loc' => url($link['loc'], array('absolute' => $absolute)),
      'id' => $link['id'],
      'type' => $link['type'],
      'langcode' => $link['language'],
    );
    $items[] = $item;
  }

  return $items;
}

function xmlsitemap_urls_prepare_url_files($absolute = TRUE) {
  $header = array(
    array('data' => t('Location'), 'field' => 'x.uri'),
    array('data' => t('Id'), 'field' => 'x.fid'),
    array('data' => t('Type'), 'field' => 'x.type'),
    array('data' => t('Language')),
  );

  $query = db_select('file_managed', 'x');
  $query->addField('x', 'type');
  $query->addField('x', 'uri', 'loc');
  $query->addField('x', 'fid', 'id');
  $query->condition('status', 1)
    ->condition('uri', '%private://%', 'NOT LIKE');
  $query->extend('TableSort')->orderByHeader($header);
  $links = $query->execute();

  $items = [];
  while ($link = $links->fetchAssoc()) {
    $alias = $link['loc'];
    $parsed_url = drupal_parse_url($alias);
    $link['loc'] = $parsed_url['path'];
    $url = file_create_url($link['loc']);
    if (!$absolute) {
      $path = parse_url(file_create_url($link['loc']));
      $url = $path['path'];
    }
    if(file_exists($alias)) {
      $item = [
        'loc' => $url,
        'id' => $link['id'],
        'type' => $link['type'],
        'langcode' => 'und',
      ];
      $items[] = $item;
    }
  }
  return $items;
}

function xmlsitemap_urls_export_overview() {
  $header = array(
    array('data' => t('Location'), 'field' => 'x.loc', 'sort' => 'asc'),
    array('data' => t('Id'), 'field' => 'x.id', 'sort' => 'asc'),
    array('data' => t('Type'), 'field' => 'x.type', 'sort' => 'asc',),
    array('data' => t('Language'))
  );

  $rows = [];
  $items = _xmlsitemap_url_export_get_items(TRUE);
  foreach ($items as $row) {
    $row['loc'] = l($row['loc'], $row['loc'], array('attributes' => array('target' => '_blank')));
    $rows[] = array('data' => ($row));
  }
  return [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  ];
}

function _xmlsitemap_urls_export_download_csv() {
  drupal_add_http_header('Content-Type', 'application/csv');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="file.csv"');
  drupal_send_headers();

  $items = _xmlsitemap_url_export_get_items();
  print implode(',', array('Location', 'Id', 'Type', 'Language')) . ',' . "\n";
  foreach ($items as $item) {
    $row = '"' . $item['loc'] . '"' . ',';
    $row .= '"' . $item['id'] . '"' . ',';
    $row .= '"' . $item['type'] . '"' . ',';
    $row .= '"' . $item['langcode'] . '"' . ',';
    $row .= "\n";
    print $row;
  }
  exit;
}

/**
 * Generate a list with links.
 */
function xmlsitemap_urls_export_links_generate() {
  drupal_add_http_header('Content-Type', 'text/plain');
  $items = _xmlsitemap_url_export_get_items($absolute = FALSE);
    $links = array_column($items, 'loc');
    $text = implode("\n", $links);
    echo $text;
}
